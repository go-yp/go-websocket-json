module gitlab.com/go-yp/go-websocket-json

go 1.14

require (
	github.com/gorilla/websocket v1.4.2
	github.com/valyala/fastjson v1.6.3
)
