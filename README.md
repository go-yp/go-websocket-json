# Go WebSocket JSON

### Example
* [https://gowebexamples.com/websockets/](https://gowebexamples.com/websockets/)

### Run
```bash
go mod vendor
```
```bash
go run main.go
```
```bash
browse localhost:8080
```