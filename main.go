package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/websocket"
	"github.com/valyala/fastjson"
	"net/http"
)

type CommandHeader struct {
	CommandName string `json:"command_name"`
}

type Command struct {
	CommandName string          `json:"command_name"`
	Value       json.RawMessage `json:"value"`
}

type AddStringCommand struct {
	Value string `json:"value"`
}

type AddCompany struct {
	CompanyName string `json:"company_name"`
	SiteURL     string `json:"site_url"`
}

type AddCompanyCommand struct {
	Value AddCompany `json:"value"`
}

func ParseMessageV1(message []byte) error {
	var header CommandHeader

	var unmarshalErr = json.Unmarshal(message, &header)
	if unmarshalErr != nil {
		return unmarshalErr
	}

	switch header.CommandName {
	case "add_string_command":
		var value AddStringCommand

		var unmarshalErr = json.Unmarshal(message, &value)
		if unmarshalErr != nil {
			return unmarshalErr
		}

		fmt.Printf("[SUCCESS] Parse v1 command %s with value %+v\n", header.CommandName, value.Value)
	case "add_company_command":
		var value AddCompanyCommand

		var unmarshalErr = json.Unmarshal(message, &value)
		if unmarshalErr != nil {
			return unmarshalErr
		}

		fmt.Printf("[SUCCESS] Parse v1 command %s with value %+v\n", header.CommandName, value.Value)
	default:
		return errors.New(fmt.Sprintf("undefined command %s", header.CommandName))
	}

	return nil
}

func ParseMessageV2(message []byte) error {
	var command Command

	var unmarshalErr = json.Unmarshal(message, &command)
	if unmarshalErr != nil {
		return unmarshalErr
	}

	switch command.CommandName {
	case "add_string_command":
		var value string

		var unmarshalErr = json.Unmarshal(command.Value, &value)
		if unmarshalErr != nil {
			return unmarshalErr
		}

		fmt.Printf("[SUCCESS] Parse v2 command %s with value %+v\n", command.CommandName, value)
	case "add_company_command":
		var value AddCompany

		var unmarshalErr = json.Unmarshal(command.Value, &value)
		if unmarshalErr != nil {
			return unmarshalErr
		}

		fmt.Printf("[SUCCESS] Parse v2 command %s with value %+v\n", command.CommandName, value)
	default:
		return errors.New(fmt.Sprintf("undefined command %s", command.CommandName))
	}

	return nil
}

func ParseMessageV3(message []byte) error {
	var commandName = fastjson.GetString(message, "command_name")

	switch commandName {
	case "add_string_command":
		var value AddStringCommand

		var unmarshalErr = json.Unmarshal(message, &value)
		if unmarshalErr != nil {
			return unmarshalErr
		}

		fmt.Printf("[SUCCESS] Parse v3 command %s with value %+v\n", commandName, value.Value)
	case "add_company_command":
		var value AddCompanyCommand

		var unmarshalErr = json.Unmarshal(message, &value)
		if unmarshalErr != nil {
			return unmarshalErr
		}

		fmt.Printf("[SUCCESS] Parse v3 command %s with value %+v\n", commandName, value.Value)
	default:
		return errors.New(fmt.Sprintf("undefined command %s", commandName))
	}

	return nil
}

func main() {
	var upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}

	http.HandleFunc("/echo", func(w http.ResponseWriter, r *http.Request) {
		conn, upgradeErr := upgrader.Upgrade(w, r, nil)
		if upgradeErr != nil {
			fmt.Printf("[ERROR] WebSocket upgrade %+v\n", upgradeErr)

			// NOP

			return
		}

		for {
			// Read message from browser
			messageType, message, readMessageErr := conn.ReadMessage()
			if readMessageErr != nil {
				fmt.Printf("[ERROR] WebSocket read message %+v\n", readMessageErr)

				// NOP

				return
			}

			// Print the message to the console
			fmt.Printf("[INFO] WebSocket %s sent: %s\n", conn.RemoteAddr(), string(message))

			{
				var parseErr = ParseMessageV1(message)
				if parseErr != nil {
					fmt.Printf("[ERROR] Parse v1 %+v\n", parseErr)
				}
			}

			{
				var parseErr = ParseMessageV2(message)
				if parseErr != nil {
					fmt.Printf("[ERROR] Parse v2 %+v\n", parseErr)
				}
			}

			{
				var parseErr = ParseMessageV3(message)
				if parseErr != nil {
					fmt.Printf("[ERROR] Parse v3 %+v\n", parseErr)
				}
			}

			// Write message back to browser
			writeMessageErr := conn.WriteMessage(messageType, message)
			if writeMessageErr != nil {
				fmt.Printf("[ERROR] WebSocket write message %+v\n", writeMessageErr)

				// NOP

				return
			}
		}
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "index.html")
	})

	http.ListenAndServe(":8080", nil)
}
